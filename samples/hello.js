function message() {
  return "Hello World!"
}

function main(params) {

  return JSON.stringify({
    message: message(),
    total: 42,
    authors: ["@k33g_org"],
    context: "this is a demo",
    params: params.getString("name")
  })
}

function index() {
  return `
    <!doctype html>
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>👋 World</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>
        .container
        {
          min-height: 100vh;
          display: flex;
          justify-content: center;
          align-items: center;
          text-align: center;
        }
        .title
        {
          font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif;
          display: block;
          font-weight: 300;
          font-size: 100px;
          color: #35495e;
          letter-spacing: 1px;
        }
      </style>
    </head>
    <body>
      <section class="container">
          <h1 class="title">

          </h1>
      </section>
    </body>
    <script>
      //document.querySelector("h1").innerHTML="Hello World"

      fetch(document.location.pathname, { // ex: funk/yo
        method: 'post',
        body: JSON.stringify({name:"Bob Morane"}),
        headers : {
          "Content-Type": "application/json",
          "Accept": "application/json"
        }
      })
      .then(response => response.text())
      .then(text => {
        console.log(text)
        document.querySelector("h1").innerHTML=JSON.parse(text).message
      })

    </script>
    </html>
  `
}

