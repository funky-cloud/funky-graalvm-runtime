package garden.bots.funky

import io.vertx.ext.web.RoutingContext
import org.graalvm.polyglot.Context
import org.graalvm.polyglot.Source


class Kompilo {
  var language: String = ""
  val scriptContext = Context.newBuilder().allowAllAccess(true).build()

  fun compileFunction(functionCode: String, language: String) : Result<Any> {
    this.language = language
    return compileTargetLanguageFunction(functionCode)
  }

  private fun compileTargetLanguageFunction(functionCode: String) : Result<Any> {
    return try {
      Result.success(scriptContext.eval(Source.create(language, functionCode)))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }

  fun invokeFunction(name: String?, params: Any) : Result<Any> {
    return invokeTargetLanguageFunction(name, params)
  }

  fun invokeFunction(name: String?, params: Any, context: RoutingContext) : Result<Any> {
    return invokeTargetLanguageFunction(name, params, context)
  }

  fun invokeFunction(name: String?) : Result<Any> {
    return invokeTargetLanguageFunction(name)
  }

  private fun invokeTargetLanguageFunction(name: String?, params: Any) : Result<Any> {
    return try {
      Result.success(scriptContext.getBindings(this.language).getMember(name).execute(params))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }

  private fun invokeTargetLanguageFunction(name: String?, params: Any, context: RoutingContext) : Result<Any> {
    //println("🧁 language: ${this.language}")
    //println("🧁 name: ${name}")
    //println("🧁 params: ${params}")
    return try {
      val bindings = scriptContext.getBindings(this.language)
      //println("🧁 bindings: ${bindings}")
      val member = bindings.getMember(name)
      //println("🧁 member: ${member}")
      val res = member.execute(params, context)
      //println("🧁 res: ${res}")

      Result.success(res)
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }

  private fun invokeTargetLanguageFunction(name: String?) : Result<Any> {
    return try {
      Result.success(scriptContext.getBindings(this.language).getMember(name).execute())
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }

}
