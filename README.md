# Tiny Faas: a GraalVM FaaS JavaScript runtime for Kubernetes

## How to use it

You can create JavaScript functions

### JavaScript

> hello.js

```javascript
function hello(params) {
  return {
    message: "Hello World",
    total: 42,
    author: "@k33g_org",
    params: params.getString("name")
  }
}
// params is a io.vertx.core.json.JsonObject
```

> run this:

```bash
service="javascript-hello"
namespace="k-apps"

# create or update the service
kn service create --force ${service} \
--namespace ${namespace} \
--env FUNCTION_NAME="hello" \
--env LANG="js" \
--env README="# this is a JavaScript function" \
--env FUNCTION_CODE="$(cat ./hello.js)" \
--env CONTENT_TYPE="application/json;charset=UTF-8" \
--image registry.gitlab.com/borg-collective/7of9:latest
```

> Call the function

```bash
curl -d '{"name":"Bob"}' \
-H "Content-Type: application/json" \
-X POST http://javascript-hello.k-apps.192.168.64.70.xip.io
```

## References

- [Compile and Run a Polyglot Application](https://www.graalvm.org/docs/reference-manual/embed/)
