#!/bin/bash

function deploy() {
  PORT=$1 LANG="js" \
  FUNCTION_CODE="$(cat ./samples/javascript/hello.js)" \
  java -jar ./target/funky-graalvm-runtime-1.0.0-SNAPSHOT-fat.jar & echo $!
}

function destroy() {
  echo "🖐️ function with id:$FUNCTION_ID will be destroyed in $1 secs."
  sleep $1
  kill -9 $2
}

function call_destroy() {
  FUNCTION_ID=$1
  echo "🚀 function with id:$FUNCTION_ID is started and is listening on http port: $PORT"
  destroy 3600 $FUNCTION_ID &
}

{
  deploy 9091
} &> /dev/null
call_destroy $!
{
  deploy 9092
} &> /dev/null
call_destroy $!
{
  deploy 9093
} &> /dev/null
call_destroy $!

