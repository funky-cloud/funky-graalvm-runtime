#!/bin/bash

function deploy() {
  PORT=$1 LANG="js" \
  FUNCTION_CODE="$(cat ./samples/javascript/hello.js)" \
  java -jar ./target/funky-graalvm-runtime-1.0.0-SNAPSHOT-fat.jar & echo $!
}

function destroy() {
  delay=$1
  function_id=$2
  project=$3
  port=$4
  echo "🖐️ function with id:$function_id will be destroyed in $delay secs."
  sleep $delay
  kill -9 $function_id
  rm ./$project.$port
}

function call_destroy() {
  FUNCTION_ID=$1
  project=$2
  port=$3
  echo "🚀 function with id:$FUNCTION_ID is started and is listening on http port: $PORT"
  echo "" > ./$project.$port
  destroy 30 $FUNCTION_ID $project $port &
}

# -------------------------------------------
PROJECT="yo"
PORT=9001
{
  deploy $PORT
} &> /dev/null
call_destroy $! $PROJECT $PORT
# -------------------------------------------

# -------------------------------------------
PROJECT="hey"
PORT=9002
{
  deploy $PORT
} &> /dev/null
call_destroy $! $PROJECT $PORT
# -------------------------------------------

# -------------------------------------------
PROJECT="ola"
PORT=9003
{
  deploy $PORT
} &> /dev/null
call_destroy $! $PROJECT $PORT
# -------------------------------------------

